<?php

namespace Helium\PasswordManager\Rules;

use Illuminate\Contracts\Validation\Rule;

class PasswordLowercaseCharacterRule extends PasswordMustContainRule
{
	public function __construct(int $minOccurrences = 1)
	{
		parent::__construct('/[a-z]/', $minOccurrences);
	}

	public function message()
	{
		return trans('password_manager::error.lowercase', [
			'count' => $this->minOccurrences
		]);
	}
}