<?php

namespace Helium\PasswordManager;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
	public function boot()
	{
		$this->loadTranslationsFrom(__DIR__ . '/Lang', 'password_manager');

		$this->publishes([
			__DIR__ . '/Lang' => resource_path('lang/vendor/password_manager'),
		]);
	}
}