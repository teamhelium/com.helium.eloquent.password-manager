# Eloquent Password Manager
Most Laravel and Lumen apps include some amount of standard logic for storing, checking, and updating users' passwords.
This package aims to remove that boilerplate code from your project and separate it as its own module.

## Installation
`composer require helium/eloquent-password-manager`

## Basic usage
On your `User` model class, implement the interface `PasswordNotifiable` and add the trait `ManagesPasswords`.
You must also implement the interface function `sendPasswordResetNotification(string $token)`. The other interface
function, `updatePassword(string $password, string $password_confirm)` is implemented in the `ManagesPasswords` trait.

```
class User extends AuthModel implements PasswordNotifiable
{
    use ManagesPasswords;

    public function sendPasswordResetNotification(string $token)
    {
        ...
    }

    ...
}
```

## Multi-Auth User
You can also use this trait in more complex applications which allow single users to log in using multiple email
addresses.

```
class User extends AuthModel
{
    use ManagesPasswords;

    public static function notifiableModel()
    {
        return UserEmail::class;
    }
}
```
```
class UserEmail extends Model implements PasswordNotifiable
{    
    public $email;

    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function updatePassword(string $password, string $password_confirm)
    {
        $this->user->updatePassword($password, $password_confirm);
    }

    public function sendResetPasswordNotification(string $token)
    {
        ...
    }
}
```

Now, when resetting a users' password, `ManagesPassword` will search for the email address using the `UserEmail` model
instead of the `User` model.